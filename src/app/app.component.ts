import {Component, ViewChild} from '@angular/core';
import {Nav, Platform} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {Storage} from '@ionic/storage';
import _ from 'lodash';

import {HomePage} from '../pages/home/home';
import {SettingsPage} from "../pages/settings/settings";
import {HistoryPage} from "../pages/history/history";


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any;

  pages: Array<{ title: string, component: any }>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, private storage: Storage) {

    this.checkInitialCountersData();
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      {title: 'Главная', component: HomePage},
      {title: 'История', component: HistoryPage},
      {title: 'Настройки', component: SettingsPage}
    ];
  }

  /**
   * @description This method verifies existence the initial data of the counters
   */
  checkInitialCountersData() {
    this.storage.get('startMetersData').then((res) => {
      this.rootPage = _.isEmpty(res) ? SettingsPage : HomePage;
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
