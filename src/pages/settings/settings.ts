import {Component} from '@angular/core';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'settings',
  templateUrl: 'settings.html'
})
export class SettingsPage {
  private rates: object = {};
  private startMetersData: object = {};
  private tab: string;

  constructor(private storage: Storage) {

    this.tab = '1';

    storage.get('rates').then((res) => {
      this.rates = {...res};
    });

    storage.get('startMetersData').then((res) => {
      this.startMetersData = {...res};
    });
  }

  /**
   * @description this method saves the rates in the storage
   */
  saveRates() {
    this.storage.set('rates', this.rates);
    this.tab = '2';
  }

  /**
   * @description this method saves the initial meters reading in the storage
   */
  saveStartMetersData() {
    this.storage.set('startMetersData', this.startMetersData);
  }
}
