import {Component, OnInit} from '@angular/core';
import {Storage} from '@ionic/storage';
import _ from 'lodash';
import {MetersData} from '../../interfaces/maters-data';
import moment from 'moment';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage implements OnInit {
  private rates: any = {};
  private metersData: MetersData;
  private lastMetersData: MetersData;
  private result: object = {};
  private metersDataMap: object;

  constructor(private storage: Storage) {

    this.metersData = {
      energy: '',
      coldWater: '',
      hotWater: '',
    };

    this.lastMetersData = {...this.metersData};

    storage.get('rates').then((res) => {
      this.rates = {...res};
    });

    storage.get('metersDataMap').then((res) => {

      const now = moment();
      const key = now.month(now.month() - 1).format('MM-gggg');

      if (_.isEmpty(res)) {
        storage.get('startMetersData').then((res) => {
          this.lastMetersData = {...res};
        });
      } else {
        this.metersDataMap = res;
        this.lastMetersData = this.metersDataMap[key].metersData;
      }
    });
  }

  ngOnInit() {
    this.result = {
      energy: 0,
      coldWater: 0,
      hotWater: 0,
      display: 'first'
    };
  }

  /**
   * @description this method saves the current meters data and payments in the storage
   */
  save() {
    if (!this.metersData.energy || this.metersData.coldWater || this.metersData.hotWater) {
      return;
    }

    const key = moment().format('MM-gggg');

    this.metersDataMap[key] = {
      metersData: this.metersData,
      result: this.result,
      date: new Date()
    };

    this.storage.set('metersDataMap', this.metersDataMap);
  }

  /**
   * @description this method calculate payments
   */
  calculateSum() {
    this.result = {
      energy: (this.metersData.energy - _.get(this.lastMetersData, 'energy', 0)) * this.rates.energy,
      coldWater: (this.metersData.coldWater - _.get(this.lastMetersData, 'coldWater', 0)) * this.rates.coldWater,
      hotWater: (this.metersData.hotWater - _.get(this.lastMetersData, 'hotWater', 0)) * this.rates.hotWater,
      display: 'second'
    };
  }

}
